2DPlatformer
============

This is a litle mobile game (Android only for now) in development using Unity3D.
It will be a die & retry game with a gameplay similar to the first Rayman.

We are a group of 5 students and we work on it when we have some time.

Since Unity and Github don't really like each other, every developper works on its own branch of the project and the master will be merged at the desired time.
